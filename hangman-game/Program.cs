﻿using System.Diagnostics.Metrics;
using System.Linq;
using AsciiArt;

bool hasWin(string word, List<char> letters)
{
    bool result = true;
    char[] wordSplitted = word.ToCharArray();

    foreach (var letter in wordSplitted)
    {
        if (!letters.Contains(letter))
        {
            result = false;
            break;
        }
    }
    return result;
}

void GuessWord(string word)
{
    const int MAX_LIFES = 6;
    int remainingLifes = MAX_LIFES;
    List<char> letters = new();
    List<char> lettersNotInWord = new();
    string wordUpper = word.Trim().ToUpper();

    Console.WriteLine(Ascii.Art[0]);

    while (remainingLifes > 0)
    {
        DisplayWord(wordUpper, letters);
        Console.WriteLine();

        if (lettersNotInWord.Count > 0)
        {
            Console.WriteLine($"Le mot ne contient pas les lettres suivantes: {String.Join(", ", lettersNotInWord)}");
        }
        char letter = AskLetter();
        letters.Add(letter);
        Console.Clear();

        string message;
        if (wordUpper.Contains(letter))
        {
            message = "Cette lettre est dans le mot!";
            if (hasWin(wordUpper, letters))
            {
                break;
            }
        }
        else
        {
            remainingLifes--;
            message = $"Cette lettre n'est pas dans le mot! Vous avez perdu une vie.\nVies restantes: {remainingLifes}";
            if (!lettersNotInWord.Contains(letter))
            {
                lettersNotInWord.Add(letter);
            }
        }

        Console.WriteLine(message);
        Console.WriteLine(Ascii.Art[MAX_LIFES - remainingLifes]);
        Console.WriteLine();
    }
  
    if(remainingLifes == 0)
    {
        Console.WriteLine("Vous avez perdu!");
    }
    else
    {
        Console.WriteLine("Vous avez gagné!");
    }
}

void DisplayWord(string word, List<char> letters)
{
    char[] wordToDisplay = new char[word.Length];

    for (int i = 0; i < word.Length; i++)
    {
        char letter = Char.ToUpper(word[i]);

        if (letters.Contains(letter))
        {
            wordToDisplay[i] = letter;
        }
        else { wordToDisplay[i] = '_'; }
    }
    Console.WriteLine(String.Join(' ', wordToDisplay));
}

string[]? WordsLoader(string fileName)
{
    try
    {
        return File.ReadAllLines(fileName);
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Erreur de lecture du fichier: {ex.Message}");
    }
    return null;
    
}

char AskLetter(string message = "Veuillez entrer une lettre: ")
{
    Console.Write(message);

    try
    {
        string? letter = Console.ReadLine();

        if (letter is null || letter.Trim() == "" || letter.Trim().Length > 1)
        {
            throw new Exception();
        }
        return letter.ToUpper().Trim().ToCharArray()[0];
    }
    catch (Exception)
    {

        Console.WriteLine("Veuillez entrer une seule lettre!");
        return AskLetter();
    }


}

bool PlayAgain()
{
    while (true)
    {
        char letter = AskLetter("Souhaitez-vous rejouer ? (o/n)");
        if (letter == 'O' || letter == 'N')
        {
            return letter == '0';
        }
    }
}

string[]? words = WordsLoader("words.txt");


if (words is not null && words.Length > 0)
{
    do
    {
        Random rand = new();
        GuessWord(words[rand.Next(0, words.Length)]);
    } while (PlayAgain());
}
else
{
    Console.WriteLine("La liste de mots est vides");
}
